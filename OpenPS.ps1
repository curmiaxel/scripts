$count = $args[0]

if ($count -lt 1 -Or $count -gt 10) {
    echo 'Please specify a number between 1 and 10'
    exit
}

for ($i = 0; $i -lt $count; $i++) {
    start PowerShell
}