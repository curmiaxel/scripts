## About

A collection of scripts, some are self-developed, and some are found online.
Currently only scripts for Windows are available, however, Unix scripts will be available in the future.  
---

## Windows

In order to run PowerShell scripts, the ExecutionPolicy has to be changed from default.
To do this, please change the ExecutionPolicy to **Bypass** with the command ```Set-ExecutionPolicy Bypass``` as an administrator.
*You can revert the ExecutionPolicy to **Default** with the command ```Set-ExecutionPolicy Default```.*

---